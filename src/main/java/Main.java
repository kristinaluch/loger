import services.MyOwnException;
import services.RandomNumberService;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();
        RandomNumberService randomNumberService = new RandomNumberService(random);

        randomNumberService.generateInt();

    }
}
