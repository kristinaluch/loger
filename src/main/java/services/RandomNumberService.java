package services;

import org.apache.log4j.Logger;

import java.util.Random;

public class RandomNumberService {


//    ������� ����������� ����������, ������� ��� ��������� ��������� ����� ����������.
//    �������� ���������, ������� ��� ������� ���������� ��������� ����� � ��������� [0, 10).
//    ���� �������� � ������ ���� ����� 5 � ��������� ����������. ����� ���������� ���������������� ����� � X�, ��� X � ��������������� �����.
//    ���� ���������� ���� ������������� � ������������ ����� ����������. ���� ���������� �� ���� � ������������ ����� ����������� ������� ��������

    private Random random;

    private static final int NUMBER_THROW_EXCEPTION = 5;

    private static final Logger LOGGER_CONSOLE = Logger.getLogger("loggerConsole");
    private static final Logger LOGGER_FILE = Logger.getLogger("loggerFile");



    private static final String MSG_EXCEPTION = "��������������� ����� � ";
    private static final String MSG_INFO = "���������� ������� ��������";

    public RandomNumberService(Random random) {
        this.random = random;
    }

    public void generateInt(){
        int number = random.nextInt(10);

        if (number<= NUMBER_THROW_EXCEPTION){
            try {
                throw new MyOwnException(MSG_EXCEPTION, number);
            } catch (MyOwnException e) {
                String logMsg = e.getMessage()+e.getNumber();
                LOGGER_CONSOLE.error(logMsg);
                LOGGER_FILE.error(logMsg);
            }
        }
        LOGGER_CONSOLE.info(MSG_INFO);
        LOGGER_FILE.info(MSG_INFO);

    }
}
