package services;

public class MyOwnException extends Exception{

    private int number;


    public MyOwnException(String message, int num){
        super(message);
        number = num;
    }

    public int getNumber(){return number;}
}
